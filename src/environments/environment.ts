// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
   firebaseConfig : {
    apiKey: "AIzaSyDNE-e6VN9Gsd1NaPvIJYu5ha2OAzuzDu0",
    authDomain: "ex1-29741.firebaseapp.com",
    databaseURL: "https://ex1-29741.firebaseio.com",
    projectId: "ex1-29741",
    storageBucket: "ex1-29741.appspot.com",
    messagingSenderId: "474985885787",
    appId: "1:474985885787:web:c8d4f5f64b97e7157d8f7e",
    measurementId: "G-6BRSN4ZV64"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.

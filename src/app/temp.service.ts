import { Injectable } from '@angular/core';
import {HttpClient, HttpErrorResponse} from '@angular/common/http';//הכנסנו לבד
import { Observable, throwError } from 'rxjs';//הכנסנו לבד
import { WeatherRaw } from './interfaces/weather-raw';
import { Weather } from './interfaces/weather';
import { tap, catchError, map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class TempService {

  private URL = "http://api.openweathermap.org/data/2.5/weather?q="; //2.5 זה הגרסה
  private KEY = "a2b4f015624cbbf37e32008cfae2c58e";
  private IMP = "&units=metric";

  searchWeatherData(cityName:String):Observable<Weather>{//יצרנו פונקציה שמקבלת משתנה עיר סטרינג ומחזירה מזג אוויר ובגלל שזה א-סינכרוני עושים אובסרוובל.
    return this.http.get<WeatherRaw>(`${this.URL}${cityName}&APPID=${this.KEY}${this.IMP}`)
    .pipe(
      map(data=>this.transformWeatherData(data)),
      catchError(this.handleError)
    )
  }
  constructor(private http:HttpClient) { }

  private handleError(res: HttpErrorResponse) {
    console.error(res.error);
    return throwError(res.error || 'Server error');
}

  private transformWeatherData(data:WeatherRaw):Weather{//gets data  type weather raw and returns in weather formatt
    return {//התכונות משמאל לנקודותיים צריכות להיות מתאימות לוויזר והתכונות מימין לנקודותיים צריכות להיות מתאימות לפורמט ווזר ראו.
      name: data.name,
      country: data.sys.country,
      image: `http:api.openweathermap.org/img/w/${data.weather[0].icon}`,//ישלים לי מילים לבד כי הוא מכיר את האינטרפייס שיצרתי.
      description: data.weather[0].description,
      temperature: data.main.temp,
      lat:data.coord.lat,
      lon:data.coord.lon
    }
  }
  
}



export interface WeatherRaw {//י פה מבנה נתונים שמכיל מערך של אובייקטים
 //ניצור דטה טייפ חדש   
        weather:[//זה מערך שמורכב מאובייקט מזג אוויר דסקריפשן ואייקון 
           { 
              description:String
              icon:String
           }
        ];
        main:{ 
           temp:Number,
        };
        sys:{ 
           country:String,
        };
        coord:{
            lon:Number,
            lat:Number
        }
        name:String;
}





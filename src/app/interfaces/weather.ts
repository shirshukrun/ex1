export interface Weather {
    name: String,
    country:String,
    image:String,
    description:String,
    temperature:Number,
    lat?:Number,
    lon?:Number
}

import { Router, ActivatedRoute } from '@angular/router';
import { BooksService } from './../books.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-bookform',
  templateUrl: './bookform.component.html',
  styleUrls: ['./bookform.component.css']
})
export class BookformComponent implements OnInit {

  constructor(private booksservice:BooksService, 
              private router:Router,
              private route: ActivatedRoute) { }

  title:string;
  author:string; 
  id:string;
  isEdit:boolean = false;
  buttonText:string = 'Add book' 
  
  onSubmit(){ 
    if(this.isEdit){
      console.log('edit mode');
      this.booksservice.updateBook(this.id,this.title,this.author);
    } else {
      this.booksservice.addBook(this.title,this.author)
    }
    this.router.navigate(['/books']);  
  }  

  ngOnInit() {
    this.id = this.route.snapshot.params.id;
    if(this.id) {
    this.isEdit = true;
    this.buttonText = 'Update book'   
    this.booksservice.getBook(this.id).subscribe(
      book => {
        console.log(book.data().author)
        console.log(book.data().title)
        this.author = book.data().author;
        this.title = book.data().title; 
      }
    )
   }
  }
}

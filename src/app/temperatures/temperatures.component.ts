import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { Weather } from '../interfaces/weather';
import { TempService } from '../temp.service';

//קטע קוד הבא נקרא דקוריישן, צריך אותו כי זה אומר שהמחלקה הזאת מייצגת קומפוננט. כל מחלקה שאני רוצה לעשות לה קומפוננט אני צריך דקוריישן. 
@Component({
  selector: 'app-temperatures',
  templateUrl: './temperatures.component.html',
  styleUrls: ['./temperatures.component.css']
})
export class TemperaturesComponent implements OnInit {

  constructor(private route: ActivatedRoute, private tempService:TempService) { }//עכשיו יש לי את טמפסרוויס בתור תכונה במחלקה

  likes:number = 0; //משתנה שאנו מוסיפים הוא תכונה של המחלקה
  //אנו רוצים להציג כמה לייקים יש, אך שמספר הלייקים יוצג רק אם הוא גבוהה מ0 ואם לא אז שלא יוצג מספר כלל
  temperature; 
  city;
  image:String;

  tempData$:Observable<Weather>;
  errorMessage:string; 
  hasError:boolean = false; 

  addLikes(){//ניצור פונקציה
    this.likes++//דיס אומר שאם אנו רוצים לפנות לתכונה לייקס שנמצאת במחלקה הזאת, נכתוב דיס נקודה ושם התכונה
  }

  ngOnInit() {
  
 // this.temperature = this.route.snapshot.params.temp;
  this.city= this.route.snapshot.params.city;
  this.tempData$ = this.tempService.searchWeatherData(this.city);
  this.tempData$.subscribe(
    data => {
      console.log(data);
      this.temperature = data.temperature;
      this.image = data.image;
    },
    error =>{
        console.log(error.message);
        this.hasError = true; 
        this.errorMessage = error.message; 
      
    }
  )
}
}

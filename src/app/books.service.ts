import { Injectable } from '@angular/core';
import {observable, Observable} from 'rxjs';
import { AngularFirestore } from '@angular/fire/firestore';

@Injectable({
  providedIn: 'root'
})
export class BooksService {

  books:any = [{title:'Alice in Wonderland', author:'Lewis Carrol'},{title:'War and Peace', author:'Leo Tolstoy'}, {title:'The Magic Mountain', author:'Thomas Mann'}];
/*
  getBooks(){//זה מדמה פונקציה במסד הנתונים
    return this.books;
  }
  //אנחנו רוצים למשוך את הרשימה מהסרוויס שיצרנו אל בוקס קומפוננט
*/
//עכשיו אנו משנים את הפונקציה
/*
getBooks(){
  setInterval(()=>this.books,1000);//כשזה שורה אחת זה עושה ריטרן בדיפולט ולא צריך לכתוב ריטורן.
}
*/

getBooks(): Observable<any[]> {
  const ref = this.db.collection('books');
  return ref.valueChanges({idField: 'id'});
}

getBook() :Observable<any[]>{
 return this.db.collection('books').valueChanges({idField:'id'});// ווליו צ'יינגס זאת פונקציה שבדיפולט מחזירה רק את האובייקט ולא את האיידי שלו
 //הפונקציה עובדת על ווליו צ'יינגס הוא לא שולח איידי, אני צריכה להוסיף משהו או להוסיף פונקציה אחרת. כדי שווליו ציינגס ישלח גם את האיידי 
}

addBook(title:string, author:string){//getting here the books and we want to sent it to the db
  const book = {title:title, author:author};
  this.db.collection('books').add(book)
}

deleteBooks(id:string){//מקבל איי די מסוג סטרינג
  this.db.doc(`book/${id}`).delete();//הפקודה הזאת מוחקת את המסמך שנמצא בבוקס איי די
}

updateBook(id:string, title:string, author:string){
  this.db.doc(`books/${id}`).update({
    title:title,//במקום פונקציית אפדייט משתמשים ב-סט
    author:author
  });

}

 constructor(private db:AngularFirestore) { }//אני בעצם אומרת פה תיצור אוביקט שנקרא דטהבייס מסוג אנגולר פיירסטור וצרף אותו כתכותה למחלקה הזאת.
}


//creating observable:
/*getBooks(){
  
  const booksObservable = new Observable(
    observer => {// הגדרנו משתנה שנכניס לתוכו פונקציית שגיאה הוא מייצג את המידע שהאובסרוובל שולח
      setInterval(
        ()=>observer.next(this.books),5000
        )
    }
  )
  return booksObservable;
}
addBooks(){
  setInterval(
    () => this.books.push({title:'A new book', author:'A new author'})
    ,5000)
}
  }*/
 
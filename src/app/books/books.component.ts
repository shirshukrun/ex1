import { Component, OnInit } from '@angular/core';
import { BooksService } from '../books.service';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-books',
  templateUrl: './books.component.html',
  styleUrls: ['./books.component.css']
})
export class BooksComponent implements OnInit {

  panelOpenState = false;
  books:any;//ההוספה לבוקס אומרת שהוא יכול לקבל כל ערך. 
  books$:Observable<any>;

deleteBook(id:string){
  this.booksservice.deleteBooks(id);//למה אין איי די? ההסבר דרך בוקס סרוויס שלנו. 
}

  constructor(private booksservice:BooksService) { }

  ngOnInit() {
    /*
    this.books = this.booksservice.getBooks().subscribe(
      (books) => this.books = books
    )
*/    //בהתחלה לא הייתה השהייה אז כשמשכתי את הבוקס, הם הגיעו לדפדפן, אבל עכשיו הפונקציה הפעילה טיימר שספר עד 1000 מיל ומחכה לתגובה מסוימת כדי להפעיל את הפונקציה
  //הפיתרון לזה הוא להשתמש באוסרוובל, זה אומר שהפונקציה תחזיר אובסרוובל במקום ערך. אנחנוו נירשם אליו
  //ברגע שנירשם אז כל מידע חדש שיופיע, גם אם יופיע יותר מפעם אחת, אנו מאזינים לאירוע הזה.
//.subscribe says that i signed up. 
     
//this.booksservice.addBooks();
this.books$ = this.booksservice.getBooks(); 

  }  

}
